<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление в корзину по коду");
?>

<form id="addToCartForm" class="js-items-form" action="<?=SITE_TEMPLATE_PATH?>/addToCart.php">
    <div class="items-wrapper">
        <div class="form-group js-wrapper-input">
            <input class="form-control js-item-field" type="text" placeholder="Ввод кода товара">
        </div>
        <div class="form-group js-wrapper-input">
            <input class="form-control js-item-field" type="text" placeholder="Ввод кода товара">
        </div>
        <div class="form-group js-wrapper-input">
            <input class="form-control js-item-field" type="text" placeholder="Ввод кода товара">
        </div>
    </div>
        <div class="form-group">
            <a id="addField" class="js-add-field" href="#" >Добавить поле</a>
        </div>

    <div class="result-wrapper">
        <div class="form-group">
            <h3 for="listXmlIdItems">Список товаров</h3>
            <div class="listItems js-list-items">

            </div>
        </div>
    </div>
    <button id="addToCartBtn" class="btn btn-primary js-add-to-cart" disabled>Добавить в корзину</button>
    <?=bitrix_sessid_post()?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
