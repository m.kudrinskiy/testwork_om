<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

$idItems = array();
$price = array();

if (!empty($_GET['term'])) { // при запросе autocomplete

    CModule::IncludeModule('iblock');
    $term = $_GET['term'];

    // Шаблон рег. выражения
    $pattern = '/^'.preg_quote($term).'/iu';

    $arSelect = Array("XML_ID", "ID", "NAME"); // выбор полей
    $arFilter = array('IBLOCK_ID' => '2', 'XML_ID' => '%'.$term.'%'); // фильтр товаров содержащих запрос кода

    $res = CIBlockElement::GetList( // выборка товаров содержащих код
        Array(),
        $arFilter,
        false,
        Array("nPageSize"=>5),
        $arSelect
    );
    while($ob = $res->GetNext()){ // запись вывранных полей
        $idItems[] = $ob['XML_ID'];
        $fields[$ob['XML_ID']]['XML_ID'] = $ob['XML_ID'];
        $fields[$ob['XML_ID']]['ID'] = $ob['ID'];
        $fields[$ob['XML_ID']]['NAME'] = $ob['NAME'];
    }
    // формирование ответа с полученными данными
    $output='[';
    foreach ($idItems as $key => $value) {
        $output .= '{ "label" : "' . $value . '", "value" : "' . $value . '", "id" : "' . $fields[$value]['ID'] . '", "name" : "' . $fields[$value]['NAME'] . '"},';
    }
    $output = substr($output,0,-1) . ']';
    echo $output; // ответ на запрос
}

if (!empty($_POST['xml_id'] === 'y')) { // при запросе данных о цене товара по коду

    $ID = $_POST['id']; // код товара

    CModule::IncludeModule("catalog");

    $dbProductPrice = CPrice::GetListEx( // получение данных о цене
        Array(),
        Array("PRODUCT_ID" => $ID),
        false,
        false,
        Array("ID", "PRICE", "CURRENCY")
    );
    while ($arPrices = $dbProductPrice->GetNext()) { // запись цены
        $dbPrice = $arPrices["PRICE"];
    }

    echo $dbPrice; // ответ на запрос цены товара
}

if (!empty($_POST['ajax_basket'] === 'Y')) { // при запросе добавления в корзину

    $action = $_POST['action'];

    CModule::IncludeModule("sale");

    if (CModule::IncludeModule("catalog")) {

        foreach ($_POST['data'] as $key => $product) { // добавление товаров из списка в корзину

            if (($action == "ADD2BASKET") && IntVal($product['id']) > 0) {
                $elem_quantity = $product['quantity'] || 1;
                $elem_id = $product["id"];
                $elem_name = $product["name"];
                $elem_price = $product["price"];

                $arFields = array( // поля для добавления в корзину
                    "PRODUCT_ID" => $product["id"],
                    "PRICE" => $product["price"],
                    "CURRENCY" => "RUB",
                    "QUANTITY" => $product['quantity'] || 1,
                    "LID" => "s1",
                    "CAN_BUY" => "Y",
                    "NAME" => $product["name"]
                );
                $arProps = array();
                $arFields["PROPS"] = $arProps;

                if (CSaleBasket::Add($arFields)) {
                    echo $product["value"]; // ответ с кодом товара добавленного в корзину
                };
            }
        }
    }
}
