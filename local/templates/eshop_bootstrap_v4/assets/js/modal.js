$(document).ready(function () {
    $(document).on('click', '.close', function () {
        $('.modal-popup').hide();
        $('#errorSendForm').trigger('reset').hide();
        $('#textSuccess').hide();
    });
    $(document).on('keydown', 'body', function(event) {
        if ((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey) {
            var selectedText = '';
            if (window.getSelection) {
                let selection = window.getSelection();
                selectedText = selection.toString();
            } else if (document.selection) {
                let range = document.selection.createRange();
                selectedText = range.htmlText;
            }
            if ( selectedText != '' ) selectedText = 'В данном тексте есть ошибка - "' + selectedText + '"';
            $('#textSuccess').hide();
            $('.modal-popup').show();
            $('#errorSendForm').show();
            $('#mk_popup form #textError').text(selectedText).val(selectedText).focus().blur();
            let $errorPage = location.origin + location.pathname;
            $('#mk_popup form #urlPage').text($errorPage).val($errorPage).focus().blur();

        }
    });
    $(document).on('click', '#btn-send-error', function (e) {
        e.preventDefault();
        let $form = $('#errorSendForm');
        let url = $form.attr('action');
        let urlErrorPage = $form.find('#urlPage').val();
        let textError = $form.find('#textError').val();
        BX.ajax({
            url: url,
            method: 'POST',
            data: {urlErrorPage: urlErrorPage, textError: textError},
            onsuccess: function (result) {
                console.log('success');
                if (result) {
                    $('#textSuccess').text(result).show();
                    $('#errorSendForm').trigger('reset').hide();
                }
                // $('.modal-popup').hide();
            },
            onfailure: function () {
                console.log('failure');
            }
        });
    });

})
