$(document).ready(function () {

    var $form = $('.js-items-form'); // форма ввода кодов
    var url = $form.attr('action'); // ссылка для запроса
    var $input = $('.js-item-field'); // поля ввода
    var dataProducts = []; // данные для добавления в корзину выбранных товаров

    $(function() { // инициализация плагина autocomplete

        $('.js-items-form .js-item-field').autocomplete({ // данные для подсказки из запроса
            source: url,
            select: function( event, ui ) {
                responseItem(ui.item.value, ui.item.id, ui.item.name, ui.item);
                $('.js-add-to-cart').prop('disabled', false);
            }
        });
    });

    $('.js-add-field').on('click', function (e) { // добавление нового поля ввода
        e.preventDefault();
        $('.js-wrapper-input:first').clone().appendTo('.items-wrapper').find("input").val("");
        $('.js-items-form .js-wrapper-input:last-child').find('.js-item-field').autocomplete({
            source: url,
            select: function( event, ui ) {
                responseItem(ui.item.value, ui.item.id, ui.item.name, ui.item);
                $('.js-add-to-cart').prop('disabled', false);
            }
        });
    });

    // отправка запроса данных о цене товаре
    function responseItem(value, id, name, item) {
        BX.ajax({
            url: url,
            method: 'POST',
            data: {xml_id: 'y', id: id},
            onsuccess: function (result) { // успешное получение цены
                Object.defineProperties(item, {
                    'price': {
                        value: result,
                        writable: true,
                        enumerable: true,
                        configurable: true
                    }
                });
                if (dataProducts.length > 0) { // добавляем данные в массив товаров для корзины
                    dataProducts[dataProducts.length] = item
                } else {
                    dataProducts['0'] = item;
                }
                // вывод данных о выбранном товаре в список на страницу
                $('.js-list-items').append('<div class="js-elem-item" id="' + value + '">код товара: ' + value + ' - ' + name + ' цена: ' + result + ' &#8381;' + '<div class="delete js-delete-item">x</div></div>');
            },
            onfailure: function () {
                console.log('failure');
            }
        });
    }

    $('.js-list-items').on('click', '.js-delete-item', function (e) { // удаление товара из списка
        e.preventDefault();
        $(this).parent('.js-elem-item').remove(); // удаляем строку в списке
        let value = $(this).parent('.js-elem-item').attr('id'); // получаем код для очистки поля ввода
        let $input = $('input.js-item-field').filter(function(){return this.value === value}); // находим поля ввода по коду
        $input.val(""); // очищаем поля ввода удаленного товара
        if ($('.js-elem-item').length < 1) {
            $('.js-add-to-cart').prop('disabled', true);
        }
    });

    $('.js-add-to-cart').on('click', function (e) { // событие добавления в корзину
        e.preventDefault();
        CBasket.add(dataProducts); // добавляем товары в корзину
    });

    var CBasket = {
        add: function (products) { // добавление в корзину
            if (products) {
                $.post(url, {
                    ajax_basket: 'Y',
                    action: 'ADD2BASKET',
                    data: products // добавляемые товары
                }, function (data) {
                    $.each(products, function (key, item) { // проходим по всем товарам
                        $('#'+item.value).remove(); // удаляем добавленные товары из списка
                        let $input = $('input.js-item-field').filter(function(){return this.value === item.value});
                        $input.val(""); // очищаем код добавленного товара из поля ввода
                        $('.js-add-to-cart').prop('disabled', true);
                    });
                    alert('Товары добавлены в корзину'); // сообщение об успешном добавлении товаров
                });
            }
        }
    }
})
