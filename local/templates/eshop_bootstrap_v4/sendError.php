<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;
use Bitrix\Main\Loader;

if (isset($_REQUEST["urlErrorPage"]) && isset($_REQUEST["textError"])) {

    CModule::IncludeModule('iblock');
    $el = new CIBlockElement;

    $arLoadProductArray = array(
        "MODIFIED_BY" => $USER->GetID() || $USER->GetID('1'), // элемент изменен текущим пользователем
        //"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID" => 4,
        "NAME" => $_POST["urlErrorPage"],
        "ACTIVE" => "N",
        "PREVIEW_TEXT" => $_POST["textError"],
    );

    if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
        echo "Спасибо! Ваше сообщение отправлено.";
    } else {
        echo "Произола ошибка. Попробуйте снова";
    }
} else {
    echo 'Что-то пошло не так. Попробуйте снова';
}

